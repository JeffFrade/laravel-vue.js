@extends('layouts.app')

@section('content')
    <pagina tamanho="12">
        <painel titulo="Lista de Artigos" cor="bg-light">
            <a href="#">Criar</a>

            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Título</th>
                        <th>Descrição</th>
                        <th>Autor</th>
                        <th>Data</th>
                        <th>Ações</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Título...</td>
                        <td>Descrição...</td>
                        <td>Autor</td>
                        <td>Data...</td>
                        <td>
                            <a href="#" class="btn btn-light btn-sm" title="Editar"><i class="fa fa-edit"></i></a>
                            <a href="#" class="btn btn-danger btn-sm" title="Deletar"><i class="fa fa-ban"></i></a>
                        </td>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>Título...</td>
                        <td>Descrição...</td>
                        <td>Autor</td>
                        <td>Data...</td>
                        <td>
                            <a href="#" class="btn btn-light btn-sm" title="Editar"><i class="fa fa-edit"></i></a>
                            <a href="#" class="btn btn-danger btn-sm" title="Deletar"><i class="fa fa-ban"></i></a>
                        </td>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>Título...</td>
                        <td>Descrição...</td>
                        <td>Autor</td>
                        <td>Data...</td>
                        <td>
                            <a href="#" class="btn btn-light btn-sm" title="Editar"><i class="fa fa-edit"></i></a>
                            <a href="#" class="btn btn-danger btn-sm" title="Deletar"><i class="fa fa-ban"></i></a>
                        </td>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>Título...</td>
                        <td>Descrição...</td>
                        <td>Autor</td>
                        <td>Data...</td>
                        <td>
                            <a href="#" class="btn btn-light btn-sm" title="Editar"><i class="fa fa-edit"></i></a>
                            <a href="#" class="btn btn-danger btn-sm" title="Deletar"><i class="fa fa-ban"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </painel>
    </pagina>
@endsection
