# Curso de Laravel com Vue.js

Curso aborda Laravel e Vue.js, utilzando o básico do Laravel e do Vue.js

---

Pré requisitos:

- PHP >= 7.0
- Node >= 8.0
- PHP Sqlite PDO driver
- NPM >= 5.0
- Composer

Instalar a aplicação:

- Clonar o projeto
- ```> Composer install ```
- Copiar e renomear o .env.example para .env
- ```> php artisan key:generate ```
- ```> php artisan migrate:fresh ```
- ```> npm install ```

Rodar a aplicação:

- ```> php artisan serve```